﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorAssign2
{
    class TransactionManager
    {
        public Transaction createTransaction()
        {
            Console.WriteLine("Enter the transaction ID.");
            int transactionID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the transaction amount.");
            double transactionAmount = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter the transaction date.");
            string transactionDate = Console.ReadLine();
            Transaction trans = new Transaction(transactionID, transactionAmount, transactionDate);
            return trans;
        }

        public void showTransactionDetails(Transaction trans)
        {
            Console.WriteLine("Transaction ID =" + trans.transactionID);
            Console.WriteLine("Transaction amount =" + trans.transactionAmount);
            Console.WriteLine("Transaction Date =" + trans.transactionDate);
        }
    }
}
