﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorAssign2
{
    class InitialTransaction
    {
        static void Main(string[] args)
        {
            TransactionManager ob = new TransactionManager();
            Transaction ob1 = new Transaction();
            ob1 = ob.createTransaction();
            Console.WriteLine("Transaction Output: ");
            ob.showTransactionDetails(ob1);

            Console.ReadLine();
        }
    }
}
